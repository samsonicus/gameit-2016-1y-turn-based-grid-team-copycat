using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Skelet
{
    public enum GameState
    {
        Building,
        Spawning,
        WaveIncoming
    }
}