﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skelet
{
    public abstract class Unit : MonoBehaviour, IPathNode
    {
        public abstract bool IsPassable(string key);
    }
}