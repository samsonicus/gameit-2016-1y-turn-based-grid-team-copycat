using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Skelet
{
    [Serializable]
    public class EnemyAmount
    {
        [SerializeField]
        private int amount;

        [SerializeField]
        private Enemy enemy;

        public int Amount { get { return this.amount; } }
        public Enemy Enemy { get { return this.enemy; } }
    }
}